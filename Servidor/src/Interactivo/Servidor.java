package Interactivo;
import java.awt.*;
import javax.swing.*;
//paquete que contiene la clases sockets
import java.net.*;
//paquete para manipular los flujos de datos
import java.io.*;
public class Servidor extends JFrame
{  
    //Se declaran los objectos necesarios para la comunicación
    ServerSocket server;
    Socket conexion;
    DataOutputStream output;
    DataInputStream input;
    // variables para el procesamiento
    double s1,s2,suma;
    JTextArea display;
    public Servidor()
    {
        super("Server");
        display= new JTextArea(20,5);
        add("Center",display);
        setSize(400,300);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public void runServer()
    {
        try
        {
            // creación del socket para atender las peticiones del cliente
            server= new ServerSocket(6000,100);
            conexion=server.accept();
            display.setText("Se ha aceptado una conexión....\n");
            System.out.println("Se ha aceptado una conexión....\n");
            display.append("\nRecibiendo sumandos...\n");  
            //Creación de las intancias para la transferencia de información
            input = new DataInputStream(conexion.getInputStream());
            output = new DataOutputStream(conexion.getOutputStream());
            display.append( "\nRecibiendo el primer sumando\n");
            s1= input.readDouble();	
            display.append( "\nRecibiendo el segundo sumando");
            s2 =input.readDouble();    
            display.append("\nEnviando Resultado..\n");  
            suma= s1+s2;
            output.writeDouble(suma);	   
            display.append( "\n Transmisión terminada.\n Se cierra el socket.\n ");
            conexion.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void main( String args[]) 
    {
        Servidor s = new Servidor();
        s.runServer();
    }
}