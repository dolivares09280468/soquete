package Concurrente;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
//paquete que contiene las clases de sockets
import java.net.*;
//paquete que contiene las clases para el manejo de flujo de datos
import java.io.*;
public class Cliente extends JFrame implements ActionListener
{
    Container c;
    JTextField op1,op2;
    JLabel l1,l2,l3;
    JPanel pN,pC,pS;
    JPanel p1,p2,p3,p4;
    JButton boton;
    JTextArea display;
    public Cliente()
    {
        super("Cliente");
        setLayout(new GridLayout(4,1));
        c= getContentPane();
        c.setBackground(Color.GRAY);
        // creación de las intancias y agregación a los paneles 
        // de las componentes GUI
        // . . . . . . . .  . . . .
        Container cont=c;
        cont.setLayout(new FlowLayout());
        l1=new JLabel("Cliente");
        l2=new JLabel("primer número:");
        l3=new JLabel("Segundo número");
        op1=new JTextField(10);
        op2=new JTextField(10);
        display=new JTextArea(10,25);
        boton = new JButton(" sumar");
        cont.add(l1);
        cont.add(l2);
        op1.addActionListener(this);
        cont.add(op1);
        cont.add(l3);
        op2.addActionListener(this);
        cont.add(op2);
        cont.add(boton);
        cont.add(display);
        pack();
        this.setLocationRelativeTo(null);
        setSize(300,350);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        boton.addActionListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent e)
    {
        double s1,s2;
    	s1=Double.parseDouble(op1.getText());
    	s2=Double.parseDouble(op2.getText());
    	System.out.println("Haciendo petición...");
    	peticionServidor(s1,s2);
    }
    private void peticionServidor(double s1, double s2)
    {
        // declaración de un objecto para el socket cliente
        Socket client;
        // declaración de los objetos para el flujo de datos
        DataInputStream input;
        DataOutputStream output;
        double suma;    
        String Suma;
        try
        {
            // creación de la instancia del socket
            client = new Socket("172.16.14.131",6000);
            display.setText("Socket Creado....\n");
            // creación de las instancias para el flujo de datos
            input = new DataInputStream(client.getInputStream());
            output = new DataOutputStream(client.getOutputStream());
	    display.append("Enviando primer sumando\n");
            output.writeDouble(s1);
            display.append("Enviando segundo sumando\n");
            output.writeDouble(s2);
            display.append ("El servidor dice....\n\n");
            suma=input.readDouble();
            Suma= String.valueOf (suma);
            display.append("El  resultado es: "+ Suma+"\n\n");
            display.append("Cerrando cliente\n\n");
            client.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void main(String args[])
    {
        new Cliente();
    }
}